var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT r.*, a.first_name, a.last_name FROM resume r ' +
        'LEFT JOIN account a on a.account_id = r.account_id';


    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(resume_id, callback) {
   var query = 'SELECT r.*, a.first_name, a.last_name FROM resume r ' +
        'LEFT JOIN account a on a.account_id = r.account_id' +
        'Group by account_id';
    /*
    var query = 'select a.first_name, a.last_name, s.school_name, c.company_name, sk.skill_name, r.resume_id from account a ' +
        'join resume r on r.account_id = a.account_id' +
        'join account_school rs on rs.account_id = a.account_id' +
        'join school s on s.school_id = rs.school_id' +
        'join account_company ac on ac.account_id = r.account_id' +
        'join company c on c.company_id = ac.company_id' +
        'join account_skill ask on ask.account_id = r.account_id' +
        'join skill sk on sk.skill_id = ask.skill_id';
*/
    var queryData = [resume_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO resume (account_id, resume_name) VALUES (?)';

    var queryData = [params.account_id, params.resume_name];
    connection.query(query, [queryData], function(err, result) {
         //callback(err, result);
         //});

        var resume_id = result.insertId;

         var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';
         var resumeSchoolData = [];
            for (var i = 0; i < params.school_id.length; i++) {
            resumeSchoolData.push([resume_id, params.school_id[i]]);
            var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';
            var resumeCompanyData = [];
                for (var i = 0; i < params.company_id.length; i++) {
                resumeCompanyData.push([resume_id, params.company_id[i]]);
                var query = 'INSERT INTO resume_skill (skill_id, resume_id) VALUES ?';
                var resumeSkillData = [];
                    for (var i = 0; i < params.skill_id.length; i++) {
                    resumeSkillData.push([params.skill_id[i], resume_id]);
                    }
                    connection.query(query, [resumeSkillData], function (err, result) {
                        callback(err, result);
         });
         }
         }
         });
         };


exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

var resumeSchoolInsert = function(resume_id, schoolIdArray, callback){
    var query = 'INSERT INTO resume_school (school_id, resume_id) VALUES (?)';
    var resumeData = [];
    for(var i=0; i < schoolIdArray.length; i++) {
        resumeData.push([resume_id, schoolIdArray[i]]);
    }
    connection.query(query, [resumeData], function(err, result){
        callback(err, result);
    });
};
module.exports.resumeSchoolInsert = resumeSchoolInsert;

var resumeSchoolDeleteAll = function(resume_id, callback){
    var query = 'DELETE FROM resume_school WHERE resume_id = ?';
    var queryData = [resume_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
module.exports.resumeSchoolDeleteAll = resumeSchoolDeleteAll;

    var resumeCompanyInsert = function(resume_id, companyIdArray, callback){
        var query = 'INSERT INTO resume (company_id, resume_id) VALUES (?)';
        var resumeData = [];
        for(var i=0; i < companyIdArray.length; i++) {
            resumeData.push([resume_id, companyIdArray[i]]);
        }
        connection.query(query, [resumeData], function(err, result){
            callback(err, result);
        });
    };
    module.exports.resumeCompanyInsert = resumeCompanyInsert;

    var resumeCompanyDeleteAll = function(resume_id, callback){
        var query = 'DELETE FROM resume_company WHERE resume_id = ?';
        var queryData = [resume_id];
        connection.query(query, queryData, function(err, result) {
            callback(err, result);
        });
    };
    module.exports.resumeCompanyDeleteAll = resumeCompanyDeleteAll;

    var resumeSkillInsert = function(resume_id, skillIdArray, callback){
        var query = 'INSERT INTO resume_skill (skill_id, resume_id) VALUES (?)';
        var resumeData = [];
        for(var i=0; i < skillIdArray.length; i++) {
            resumeData.push([skillIdArray[i], resume_id]);
        }
        connection.query(query, [resumeData], function(err, result){
            callback(err, result);
        });
    };
    module.exports.resumeSkillInsert = resumeSkillInsert;

    var resumeSkillDeleteAll = function(resume_id, callback){
        var query = 'DELETE FROM resume_skill WHERE resume_id = ?';
        var queryData = [resume_id];
        connection.query(query, queryData, function(err, result) {
            callback(err, result);
        });
    };
    module.exports.resumeDeleteAll = resumeSkillDeleteAll;

    var resumeInsert = function(resume_id, accountIdArray, callback){
        var query = 'INSERT INTO resume (resume_id, account_id) VALUES (?)';
        var resumeData = [];
        for(var i=0; i < accountIdArray.length; i++) {
            resumeData.push([resume_id, accountIdArray[i]]);
        }
        connection.query(query, [resumeData], function(err, result){
            callback(err, result);
        });
    };
    module.exports.resumeInsert = resumeInsert;

    var resumeDeleteAll = function(resume_id, callback){
        var query = 'DELETE FROM resume WHERE resume_id = (?)';
        var queryData = [resume_id];
        connection.query(query, queryData, function(err, result) {
            callback(err, result);
        });
    };
    module.exports.resumeDeleteAll = resumeDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE resume SET resume_name = ? WHERE resume_id = ?';
    var queryData = [params.resume_name, params.resume_id];

    connection.query(query, queryData, function(err, result) {
        //delete company_address entries for this company
        resumeDeleteAll(params.resume_id, function(err, result){

            if(params.resume_id != null) {
                //insert company_address ids
                resumeInsert(params.resume_id, params.account_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

exports.getAllResume = function(account_id, callback) {
    var query = 'CALL resume_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.edit = function(resume_id, callback) {
    var query = 'CALL resume_getinfo(?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};